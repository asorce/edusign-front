export const environment = {
  production: true,
  FRONT_END_ENDPOINT: 'http://localhost:4200/',
  API_ENDPOINT: 'http://localhost:3000/api',
};
