import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProcedureComponent} from "./process/procedure/procedure.component";
import {FileComponent} from "./process/file/file.component";

const routes: Routes = [
  { path: 'procedure', component: ProcedureComponent },
  { path: 'upload', component: FileComponent },
  { path: 'home', component: FileComponent },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: FileComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
