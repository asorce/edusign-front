import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {CoreModule} from "./core/core.module";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProcessModule} from "./process/process.module";
import {AppRoutingModule} from "./app-routing.module";
import localeFr from '@angular/common/locales/fr';
import {registerLocaleData} from "@angular/common";

registerLocaleData(localeFr);
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpClientModule,
    ProcessModule,
    ReactiveFormsModule,
  ],
  exports: [AppRoutingModule, ReactiveFormsModule, FormsModule],
  providers: [{
    provide: LOCALE_ID, useValue: 'fr-FR',
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
