import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from "../core/core.module";
import { FileComponent } from './file/file.component';
import { ProcedureComponent } from './procedure/procedure.component';
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    FileComponent,
    ProcedureComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    ReactiveFormsModule,
  ]
})
export class ProcessModule { }
