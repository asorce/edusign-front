import { Component, OnInit } from '@angular/core';
import { environment } from "../../../environments/environment";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiService } from "../../core/services/api.services";
import { LocalStorageService } from "../../core/services/local-storage.service";
import { CreatedFileDto } from "../../core/dto/files/created-file.dto";
import { CreatedProcedureDto } from "../../core/dto/procedures/created-procedure.dto";
import { FileObjectDto } from "../../core/dto/files/file-object.dto";
import { MemberDto } from "../../core/dto/members/member.dto";
import { MemberInterface } from "../../core/dto/members/member.interface";
import { MemberProcedureDto } from "../../core/dto/members/member-procedure.dto";
import { StartProcedureDto } from "../../core/dto/procedures/start-procedure.dto";

@Component({
  selector: 'app-procedure',
  templateUrl: './procedure.component.html',
  styleUrls: ['./procedure.component.scss']
})
export class ProcedureComponent implements OnInit {
  procedureForm: FormGroup;
  memberForm: FormGroup[];
  procedure: StartProcedureDto;
  createdProcedure: CreatedProcedureDto;
  constructor(
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private localStorageService: LocalStorageService,
    private router: Router,
  ) {
    // object storing the procedure from the API
    this.createdProcedure = {} as CreatedProcedureDto;
    this.procedure = {} as StartProcedureDto;
    this.memberForm = [];

    // check is there is a procedure pending
    if (this.localStorageService.get('createdProcedure')) {
      this.createdProcedure = JSON.parse(this.localStorageService.get('createdProcedure'));
    }
  }

  ngOnInit(): void {
    // if there is no file uploaded to the API -> redirect to upload one
    if (this.localStorageService.get('createdFile') === '') {
      this.router.navigateByUrl('/upload');
    }
    if (this.createdProcedure) {
      if (typeof this.createdProcedure.id !== "undefined") {
        // the id from yousign contains a starting "/" -> removing it
        const id = this.createdProcedure.id !== '' ? this.createdProcedure.id.slice(1) : '';
        this.apiService.getProcedure(id).subscribe((updatedProcedure) => {
          this.createdProcedure = updatedProcedure;
          this.localStorageService.set('createdProcedure', JSON.stringify(this.createdProcedure));
        });
      }
    }
    this.resetFormGroup();
  }

  /**
   * Check if there is a procedure pending
   */
  procedureCreated(): boolean {
    return Object.keys(this.createdProcedure).length > 0;
  }

  checkIfProcedureFinished(): boolean {
    return this.createdProcedure.status === 'finished';
  }

  getDateProcedureFinished(): string {
    return this.createdProcedure.finishedAt;
  }

  /**
   * The procedure is store as a form
   * We build the form with validators here
   */
  resetFormGroup(): void {
    this.procedureForm = this.formBuilder.group({
      name: new FormControl(this.procedure.name, [
        Validators.required,
      ]),
      description: new FormControl(this.procedure.description, [
        Validators.required,
      ]),
      members: this.formBuilder.array([])
    });
    this.addMember();
  }

  /**
   * There can we several members on the procedure
   * Here on click one is added to the FormArray
   */
  newMember(): FormGroup {
    return this.formBuilder.group({
      firstName: new FormControl(this.procedure.name, [
        Validators.required,
      ]),
      lastName: new FormControl(this.procedure.description, [
        Validators.required,
      ]),
      email: new FormControl(this.procedure.members, [
        Validators.required,
        Validators.email
      ]),
      phone: new FormControl(this.procedure.members, [
        Validators.required,
        Validators.pattern('[0-9]{10}'),
      ]),
    });
  }

  /**
   * Accessor to get members from the formBuilder / array
   */
  get members(): FormArray {
    return this.procedureForm.get('members') as FormArray;
  }

  /**
   * Checks the max number of members to add
   * Then if ok pushes it into the formArray
   */
  addMember(): void {
    if (this.members.length < 2) {
      this.members.push(this.newMember());
    }
  }

  /**
   * Clearing the procedure from localStorage
   * (but not clearing it from the API)
   */
  clearProcedure(): void {
    this.localStorageService.remove('createdFile');
    this.localStorageService.remove('createdProcedure');
    this.createdProcedure = {} as CreatedProcedureDto;
    this.router.navigateByUrl('/upload');
  }

  /**
   * Action to build objects containing fields and pushing object to the API
   * to start the procedure
   *
   * /!\ The form do not manage error for empty field, the last mile is missing
   */
  start(): void {
    const procedureName = this.procedureForm.get('name')?.value ?? '';
    const procedureDescription = this.procedureForm.get('description')?.value ?? '';
    if (procedureName !== '') {
      const procedureMembers = <FormArray> this.procedureForm.get('members');
      if (procedureMembers) {
        const members: MemberDto[] = [];
        // getting the fileObject from the localstorage which is filled before
        const createdFile: CreatedFileDto = JSON.parse(this.localStorageService.get('createdFile'));

        // building the fileobject conforming to the API
        // the signature position is for a portrait layout, not a landscape one ...
        const fileObject: FileObjectDto = {
          file: createdFile.id,
          page: 1,
          mention: '',
          mention2: '',
          position: "427,73,529,115",
        };
        // for a landscape -> check the size and switch to : "587,101,728,159"

        // adding each members from the formBuilder
        procedureMembers.value.forEach((member: MemberInterface) => {
          const firstname = member.firstName;
          const lastname = member.lastName;
          const email = member.email;
          const phone = member.phone;

          const memberDto: MemberDto = {
            firstname,
            lastname,
            email,
            phone,
            operationLevel : "custom",
            operationCustomModes: [ "email" ],
            operationModeEmailConfig: {
              "subject": "Votre code de sécurité",
              "content": "Pour finaliser votre signature électronique, veuillez utiliser le code de sécurité suivant: {{code}}"
            },
            fileObjects: [fileObject],
          };
          members.push(memberDto);
        });

        // building the object (from yousign documentation)
        const startProcedure: StartProcedureDto = {
          name: procedureName,
          description: procedureDescription,
          start: true,
          members
        };

        // then a call is made to the API to start the procedure with the members & fileobject
        this.apiService.startProcedure(startProcedure).subscribe((createdProcedure) => {
          this.localStorageService.set('createdProcedure', JSON.stringify(createdProcedure));
          this.createdProcedure = createdProcedure;
        });
      }
    }
  }

  /**
   * Get the members of the procedures who signed
   */
  getMembersSign(): MemberProcedureDto[] {
    if (this.createdProcedure.members && this.createdProcedure.members.length > 0) {
      return this.createdProcedure.members;
    }
    return [];
  }

  /**
   * Build the url to sign for a member of the procedure
   * @param member
   */
  getMembersSignUrl(member: MemberProcedureDto): string {
    const yousign_member_url = environment.YOUSIGN_MEMBER_SIGN_URL;
    return yousign_member_url + member.id;
  }

  /**
   * Check if a member of the procedure has signed
   * @param member
   */
  checkIfMemberHasSigned(member: MemberProcedureDto): boolean {
    return member.status === 'done';
  }
}
