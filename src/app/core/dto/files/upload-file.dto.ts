export interface UploadFileDto {
  name: string;
  file: FormData;
}
