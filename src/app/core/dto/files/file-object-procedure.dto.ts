import {FileDto} from "./file.dto";

export interface FileObjectProcedureDto {
  id: string;
  file: FileDto;
  page: number;
  position: string;
  fieldName: string;
  mention: string;
  mention2: string;
  createdAt: string;
  updatedAt: string;
  parent: string;
  reason: string;
}
