export interface FileObjectDto {
  file: string;
  page: number;
  position: string;
  mention: string;
  mention2: string;
}
