export interface CreatedFileDto {
  id: string;
  name: string;
  type: string;
  contentType: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  sha256: string;
  metadata: [];
  workspace: string;
  creator: string;
  protected: boolean;
  position: number;
  parent: string;
}
