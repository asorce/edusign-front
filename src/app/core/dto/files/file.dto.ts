export interface FileDto {
  id: string;
  name: string;
  type: string;
  contentType: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  sha256: string;
  metadata: [];
  workspace: string;
  creator: null;
  protected: false;
  position: number;
  parent: string;
}
