import {FileObjectDto} from "../files/file-object.dto";

export interface MemberDto {
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  operationLevel : string;
  operationCustomModes: [ string ],
  operationModeEmailConfig: {
    "subject": string,
    "content": string
  },
  fileObjects: FileObjectDto[];
}
