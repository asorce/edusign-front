export interface MemberInterface {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
}
