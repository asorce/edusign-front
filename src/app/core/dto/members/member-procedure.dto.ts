import {FileObjectProcedureDto} from "../files/file-object-procedure.dto";

export interface MemberProcedureDto {
  id: string;
  user: string;
  type: string;
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  position: number;
  createdAt: string;
  updatedAt: string;
  finishedAt: string;
  status: string;
  fileObjects: FileObjectProcedureDto[];
  comment: string;
  notificationsEmail: [];
  operationLevel: string;
  operationCustomModes: [];
  operationModeSmsConfig: string;
  parent: string;
}
