import {MemberDto} from "../members/member.dto";

export interface StartProcedureDto {
  name: string;
  description: string;
  start: boolean;
  members: MemberDto[];
}
