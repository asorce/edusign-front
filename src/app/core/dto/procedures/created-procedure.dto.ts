import {MemberProcedureDto} from "../members/member-procedure.dto";
import {FileProcedureDto} from "../files/file-procedure.dto";

export interface CreatedProcedureDto {
  id: string;
  name: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  finishedAt: string;
  expiresAt: string;
  status: string;
  creator: string;
  creatorFirstName: string;
  creatorLastName: string;
  workspace: string;
  template: boolean;
  ordered: boolean;
  parent: string;
  metadata: [];
  config: [];
  members: MemberProcedureDto[];
  subscribers: [];
  files: FileProcedureDto[];
  relatedFilesEnable: boolean;
  archive: boolean;
  archiveMetadata: [];
  fields: [];
  permissions: [];
}
