import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../services/api.services";
import {CreatedFileDto} from "../../dto/files/created-file.dto";
import {LocalStorageService} from "../../services/local-storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  fileName: string = '';
  formData: FormData;
  createdFileDto: CreatedFileDto;
  constructor(
    private apiService: ApiService,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {
    // the object that we get back from the API
    this.createdFileDto = {} as CreatedFileDto;

    // the formData which is going to contain the name and file uploaded to the API
    this.formData = new FormData();
    this.formData.append('name', '');

    // if we have already uploaded a file, it's store and upon reload we get it back
    if (this.localStorageService.get('createdFile')) {
      this.createdFileDto = JSON.parse(this.localStorageService.get('createdFile'));
    }
  }

  ngOnInit(): void {
  }

  /**
   * Get the input value and updates the formData to be sent afterwards to the API
   * @param event
   */
  updateFileName(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.fileName = target.value;
    this.formData.set('name', this.fileName);
  }

  /**
   * Check if the formData is valid to display a the upload button
   */
  checkFormDataValid(): boolean {
    return this.formData.get('name') !== '' && this.formData.get('file') !== null;
  }

  /**
   * Uploads the name and the PDF file to the API
   * then stores in the local storage the result
   */
  startUpload(): void {
    const upload$ = this.apiService.uploadFile(this.formData);

    upload$.subscribe((createdFileDto: CreatedFileDto) => {
      this.localStorageService.set('createdFile', JSON.stringify(createdFileDto))
      this.createdFileDto = createdFileDto;
    });
  }

  /**
   * Stores the file into formData for later upload to the API
   * @param event
   */
  onPDFSelected(event: Event): void {
    const target = event.target as HTMLInputElement;
    const files = target.files as FileList;
    if (files && files.length > 0) {
      const file: File = files[0];
      if (file) {
        this.formData.append('file', file);
      }
    }
  }

  /**
   * Checks if there is a file uploaded in which case we move
   * onto the procedure route
   */
  isFileCreated(): boolean | void {
    if (Object.keys(this.createdFileDto).length === 0) {
      return false;
    }
    this.router.navigateByUrl('/procedure');
  }
}
