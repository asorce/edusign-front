import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {UrlBuilder} from '../utils/url-builder';

@Injectable({
  providedIn: 'root'
})
export class ApiEndpointsService {
  constructor() { }

  private createUrl(
    action: string
  ): string {
    const urlBuilder: UrlBuilder = new UrlBuilder(
      environment.API_ENDPOINT,
      action
    );
    return urlBuilder.toString();
  }

  public postFileEndpoint(): string {
    return this.createUrl('files/upload');
  }
  public startProcedureEndpoint(): string {
    return this.createUrl('procedures/start');
  }
  public getProcedureEndpoint(id: string): string {
    return this.createUrl(id);
  }
}
