import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {  HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiHttpService} from './api-http.service';
import {ApiEndpointsService} from './api-endpoints.service';
import {CreatedFileDto} from "../dto/files/created-file.dto";
import {StartProcedureDto} from "../dto/procedures/start-procedure.dto";
import {CreatedProcedureDto} from "../dto/procedures/created-procedure.dto";

/**
 * The logic to make API calls is :
 *  -> getting the url from the environment
 *  -> getting the endPoint
 *  -> building a URL
 *  -> building headers for the specific method
 *  -> building a prepared call GET / POST ... for the specific method
 *    -> the caller has to subscribe to it to start the API call
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private apiHttpService: ApiHttpService,
              private apiEndpointsService: ApiEndpointsService
  ) {}

  uploadFile(uploadFile: FormData): Observable<CreatedFileDto> {
    const httpOptions = {
      headers: new HttpHeaders({
        Prefer: 'return=representation',
        'Access-Control-Allow-Origin': environment.FRONT_END_ENDPOINT,
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      })
    };
    return this.apiHttpService.post(this.apiEndpointsService.postFileEndpoint(), uploadFile, httpOptions);
  }
  startProcedure(startProcedure: StartProcedureDto): Observable<CreatedProcedureDto> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Prefer: 'return=representation',
        'Access-Control-Allow-Origin': environment.FRONT_END_ENDPOINT,
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      })
    };
    return this.apiHttpService.post(this.apiEndpointsService.startProcedureEndpoint(), startProcedure, httpOptions);
  }
  getProcedure(procedureId: string): Observable<CreatedProcedureDto> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Prefer: 'return=representation',
        'Access-Control-Allow-Origin': environment.FRONT_END_ENDPOINT,
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      })
    };
    return this.apiHttpService.get(this.apiEndpointsService.getProcedureEndpoint(procedureId), httpOptions);
  }
}

